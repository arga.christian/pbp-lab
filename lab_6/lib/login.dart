import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String dropdownValue = "Wisatawan";
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children:  [
        const Align(
          alignment: Alignment.center, // Align however you like (i.e .centerRight, centerLeft)
          child: Text(
              "Welcome",style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.bold,
          ),
            textAlign: TextAlign.center,
          ),
        ),
        const SizedBox(height: 10,),
        const Align(
          alignment: Alignment.center, // Align however you like (i.e .centerRight, centerLeft)
          child: Text(
            "What would you like to be called?",style: TextStyle(
            fontSize: 15,
          ),
            textAlign: TextAlign.center,
          ),
        ),
        const SizedBox(height: 10,),
        SizedBox(
          width: 200,
          height: 30,
          child: TextFormField(
            decoration: InputDecoration(
              fillColor: Colors.grey,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: const BorderSide(),
              ),
            ),
          ),
        ),
        const SizedBox(height: 10,),
        const Align(
          alignment: Alignment.center, // Align however you like (i.e .centerRight, centerLeft)
          child: Text(
            "Enter your password!",style: TextStyle(
            fontSize: 15,
          ),
            textAlign: TextAlign.center,
          ),
        ),
        const SizedBox(height: 10,),
        SizedBox(
          width: 200,
          height: 30,
          child: TextFormField(
            decoration: InputDecoration(
              fillColor: Colors.grey,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: const BorderSide(),
              ),
            ),
          ),
        ),
        const SizedBox(height: 10,),
        const Align(
          alignment: Alignment.center, // Align however you like (i.e .centerRight, centerLeft)
          child: Text(
            "What would you like to be known as?",style: TextStyle(
            fontSize: 15,
          ),
            textAlign: TextAlign.center,
          ),
        ),
        const SizedBox(height: 10,),
        SizedBox(
          width: 200,
          height: 30,
          child: DropdownButton<String>(
            value: dropdownValue,
            elevation: 16,
            style: const TextStyle(color: Colors.deepPurple),
            underline: Container(
              height: 2,
              color: Colors.deepPurpleAccent,
            ),
            onChanged: (String? newValue) {
              setState(() {
                dropdownValue = newValue!;
              });
            },
            items: <String>['Wisatawan', 'Pemerintah']
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          )
        ),
        const SizedBox(height: 30),
        ElevatedButton(
          onPressed: () {},
          child: const Text('login'),
        ),
      ],
    );
  }
}
