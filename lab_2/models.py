from django.db import models


# Create your models here.
class Notes(models.Model):
    to_message = models.CharField(max_length=15)
    from_message = models.CharField(max_length=15)
    title_message = models.CharField(max_length=50)
    message = models.TextField()
