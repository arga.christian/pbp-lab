from django.shortcuts import render
from .models import Notes
from django.http.response import HttpResponse
from django.core import serializers


# Create your views here.
def index(request):
    notes = Notes.objects.all()
    response = {'note': notes}
    return render(request, 'lab2.html', response)

def xml(request):
    notes = Notes.objects.all()
    data = serializers.serialize('xml', notes)

    return HttpResponse(data, content_type="application/xml")

def json(request):
    notes = Notes.objects.all()
    data = serializers.serialize('json', notes)
    return HttpResponse(data, content_type="application/json")