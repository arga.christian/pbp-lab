from django.shortcuts import render
from lab_2.models import Notes
from .forms import NotesForm
from django.http import HttpResponseRedirect


# Create your views here.
def index(request):
    note = Notes.objects.all()
    response = {'note': note}
    return render(request, 'lab_4/lab4_index.html', response)


def add_note(request):
    form = NotesForm()
    if request.method == 'POST':
        form = NotesForm(request.POST)
        form.save()
        if form.is_valid():
            return HttpResponseRedirect('/lab-4')

    response = {'form': form}
    return render(request, 'lab_4/lab4_forms.html', response)


def note_list(request):
    note = Notes.objects.all()
    response = {'note': note}
    return render(request, 'lab_4/lab4_note_list.html', response)
