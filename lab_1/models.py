from django.db import models


class Friend(models.Model):
    name = models.CharField(max_length=30)
    birth_date = models.DateField()
    npm = models.CharField(max_length=10)
